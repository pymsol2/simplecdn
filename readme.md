# SimpleCDN

Php Framework (7.4).

Briefly explain the project...(pending)

## Authors

-   Jaime Aguilá Sánchez (PruebaRana)

## 🔖 Licence

SimpleCDN is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.

## 🚚 Installation

SimpleCDN requires:

-   PHP 7.4 or greater with curl and GD or imagick

Composer:

-   Apache/log4php 2.3.0.
-   matthiasmullie/minify 1.3
-   enshrined/svg-sanitize 0.13.3

### 1 - via GIT clone

```
git clone https://gitlab.com/Pymsol/simplecdn.git
```

### 2 - Edit configuration

```
cd src\Config
edit Config.php
```

## 3 - Installation of libraries from composer

```
composer install
composer update --no-dev -o
```

# Homework

De momento esta en castellano, esta parte desaparecera cuando solo este pendiente la tarea de la web

-   [x] Añadir control bots
-   [x] Añadir gzip
-   [x] Añadir resize
-   [x] Añadir optimizacion img
-   [x] Añadir optimizacion css
-   [x] Añadir optimizacion js
-   [x] Añadir generacion webp
-   [x] Añadir optimizacion svg
-   [ ] Añadir generacion bundles (abandonado de momento)

---

-   [x] Arreglar el contenttype cuando el servidor remoto no lo envia (guardamos un mensaje de error para que se arregle la configuracion del servidor)
-   [x] Insertar allow origin host, cuando el cliente y el remoto sean identicos (somos MitM) y no exista dicha cabecera
-   [x] no enviar host, para evitar la seguridad hotlinked,
-   [x] Tres log (error, info y debug) y que sean por dias
-   [x] Contenido externo con el dominio original, para poder clasificar

---

-   [x] Controlar el host tanto por carpeta como por DNS (mycdn.com/bob.com/resource = cdn.bob.com/resource)
-   [x] Evitar las proteccion de hotlinked en las imagenes con algunos dominios protegidos
-   [x] Eliminacion de codigo muerto que no aportaba nada
-   [x] Cambios para seguir el standar PSR12 en el codigo
-   [x] Control del contenido externo mediante el referer y el host

---

-   [x] Incluir header link para indicar la url canonical
-   [x] Modificar robots.txt
-   [x] Incluir RequestChecker para decidir si esta la peticion esta permitida, y si es interno/externo, ademas de dejar un log debug

---

-   [x] Errores en peticiones sin querystring
-   [x] Errores en la decision del nombre del recurso cuando es externo

---

-   [x] Mejorar la minificacion, ver si hay opciones de mejora
-   [x] Doblar el resourceImage, para implementar GD y Imagick, comprobacion de que el servidor lo tiene instalado
-   [x] Cambiar la optimizacion de imagenes de GD a ImagicK, para quitar metadatos
-   [x] Comprobar si imagick permite el formato webp

---

-   [x] config EXTENSION_IMAGES (IMAGICK / GD)
-   [x] config COMPRESION_JPG AND COMPRESION_PNG

---

-   [x] Incluir ttf
-   [x] incluir jfif
-   [x] No crear webp de ico
-   [x] No crear webp de ico
-   [x] No optimizar heic

---

-   [x] Fix error transparent PNG
-   [x] Fix error webp in Safari

---

-   [x] Fix error SVG
-   [x] send header cache-control

---
-   [x] Fix url with measures exist
---

-   [ ] Error 404 para imagenes que no existen en el origen
-   [ ] Invalidate cache CDN
-   [ ] Docker
-   [ ] Comprobar las rutas internas en CSS, añadiendo el cdn
-   [ ] Preparar una web para acceso y configuracion

...
