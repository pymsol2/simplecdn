<?php

namespace Pymsol\SimpleLogger;

use Logger;

/*
 *    http://logging.apache.org/log4php/
 */

class Log
{

    public function getLogger(string $nombre)
    {
        if (LOGGER == 'LOG4PHP') {
            Log::configure();
            return Logger::getLogger($nombre);
        }

        return $this;
    }

    public function debug(string $message)
    {
        // Do nothing
    }

    public function error(string $message)
    {
        // Do nothing
    }
    public function info(string $message)
    {
        // Do nothing
    }

    protected static function configure()
    {
        Logger::configure(
            array(
                'rootLogger' => array(
                    'level' => 'DEBUG',
                    'appenders' => array('console', 'fileError', 'fileInfo', 'fileDebug'),
                ),
                'appenders' => array(
                    'fileError' => array(
                        'class' => 'LoggerAppenderDailyFile',
                        'layout' => array(
                            'class' => 'LoggerLayoutPattern',
                            'params' => array(
                                'conversionPattern' =>
                                '%date{Y-m-d H:i:s,u} [%pid] %-5level [%logger] %message (%relative) From:%server{REMOTE_ADDR} Request:[%request]%newline%ex',
                            ),
                        ),
                        'params' => array(
                            'datePattern' => 'Y-m-d',
                            'file' => ROOT . '/logs_cdn/SimpleCDN_Error-%s.log',
                        ),
                        'filters' => array(
                            array(
                                'class' => 'LoggerFilterLevelRange',
                                'params' => array(
                                    'levelMin' => 'WARN'
                                )
                            )
                        )
                    ),
                    'fileInfo' => array(
                        'class' => 'LoggerAppenderDailyFile',
                        'layout' => array(
                            'class' => 'LoggerLayoutPattern',
                            'params' => array(
                                'conversionPattern' =>
                                '%date{Y-m-d H:i:s,u} [%pid] %-5level [%logger] %message (%relative) From:%server{REMOTE_ADDR} Request:[%request]%newline%ex',
                            ),
                        ),
                        'params' => array(
                            'datePattern' => 'Y-m-d',
                            'file' => ROOT . '/logs_cdn/SimpleCDN_Info-%s.log',
                        ),
                        'filters' => array(
                            array(
                                'class' => 'LoggerFilterLevelRange',
                                'params' => array(
                                    'levelMin' => 'INFO',
                                    'levelMax' => 'INFO',
                                )
                            )
                        )
                    ),
                    'fileDebug' => array(
                        'class' => 'LoggerAppenderDailyFile',
                        'layout' => array(
                            'class' => 'LoggerLayoutPattern',
                            'params' => array(
                                'conversionPattern' =>
                                '%date{Y-m-d H:i:s,u} [%pid] %-5level [%logger] %message (%relative) From:%server{REMOTE_ADDR} Request:[%request]%newline%ex',
                            ),
                        ),
                        'params' => array(
                            'datePattern' => 'Y-m-d',
                            'file' => ROOT . '/logs_cdn/SimpleCDN_Debug-%s.log',
                        ),
                        'filters' => array(
                            array(
                                'class' => 'LoggerFilterLevelRange',
                                'params' => array(
                                    'levelMin' => 'DEBUG',
                                    'levelMax' => 'DEBUG',
                                )
                            )
                        )
                    ),
                    'console' => array(
                        'class' => 'LoggerAppenderConsole',
                        'layout' => array(
                            'class' => 'LoggerLayoutSimple',
                        ),
                    ),
                ),
            )
        );
    }
}
