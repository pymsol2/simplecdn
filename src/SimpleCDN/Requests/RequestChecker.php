<?php

namespace Pymsol\SimpleCDN\Requests;

use Pymsol\Utilities\Bots;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Headers\Headers;
use Pymsol\SimpleCDN\Requests\Request;
use Pymsol\SimpleCDN\Exceptions\HostNotAllowedException;
use Pymsol\SimpleCDN\Exceptions\ExtensionNotAllowedException;

class RequestChecker
{
    private $log;

    private $request;
    private $isExternal;

    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->isExternal = false;
    }

    public function checks(Request $request, $extension)
    {
        // security checks
        $this->request = $request;
        $this->checkExtensionAllowed($extension);
        $this->checkHostAllowed();
    }
    public function getIsExternal()
    {
        return $this->isExternal;
    }

    private function checkExtensionAllowed($extension)
    {
        if (!array_key_exists($extension, CDN_EXTENSIONS_WITH_EXPIRES)) {
            throw new ExtensionNotAllowedException();
        }
    }
    private function checkHostAllowed()
    {
        if ($this->checkHostAllowedAndExternalResource()) {
            return;
        }

        if ($this->checkValidBots()) {
            return;
        }

        $hostAndReferer = $this->request->getHost() . ($this->request->getHostReferer() != null ? ' - ' . $this->request->getHostReferer() : '');
        $this->log->debug('NOPE: Referer: ' . $this->request->getHostReferer() . ', Domain: ' . $this->request->getDomain() . ', Host: ' . $this->request->getHost());
        throw new HostNotAllowedException('Host or referer not allowed. [' . $hostAndReferer . ']');
    }
    private function checkHostAllowedAndExternalResource()
    {
        $res = false;
        if ($this->checkValidReferer()) {
            $res = true;
        }
        if (!$res && $this->checkValidHost()) {
            $res = true;
        }
        if (!$res && $this->checkValidDomain()) {
            $res = true;
        }
        return $res;
    }
    private function checkValidReferer()
    {
        $isValidReferer = $this->isValidDomain($this->request->getHostReferer());
        if (!$isValidReferer) {
            return false;
        }
        $isValidDomain = $this->isValidDomain($this->request->getDomain());
        if (!$isValidDomain && $this->request->getDomain() == $this->request->getHost()) {
            return false;
        }

        if ($isValidDomain) {
            $this->isExternal = ($this->request->getDomain() != $this->request->getHost());
            $this->log->debug('VALID: Referer valid: ' . $this->request->getHostReferer() . ', Domain valid: ' . $this->request->getDomain() . ', Host: ' . $this->request->getHost() . ', resource: Internal');
            return true;
        }

        $this->isExternal = $this->areDifferent($this->request->getHostReferer(), $this->request->getHost());
        $this->log->debug('VALID: Referer valid: ' . $this->request->getHostReferer() . ', Domain: ' . $this->request->getDomain() . ', Host: ' . $this->request->getHost() . ', resource: ' . ($this->isExternal ? 'External' : 'Internal'));
        return true;
    }
    private function checkValidHost()
    {
        $isValidHost = $this->isValidDomain($this->request->getHost());
        if (!$isValidHost) {
            return false;
        }

        $this->isExternal = false;
        $this->log->debug('VALID: Referer: ' . $this->request->getHostReferer() . ', Domain: ' . $this->request->getDomain() . ', Host valid: ' . $this->request->getHost() . ', resource: Internal');
        return true;
    }
    private function checkValidDomain()
    {
        $isValidDomain = $this->isValidDomain($this->request->getDomain());
        if (!$isValidDomain) {
            return false;
        }

        $this->isExternal = true;
        $this->log->debug('VALID: Referer: ' . $this->request->getHostReferer() . ', Domain valid: ' . $this->request->getDomain() . ', Host: ' . $this->request->getHost() . ', resource: External');
        return true;
    }
    private function checkValidBots()
    {
        $bots = new Bots();
        $userAgent = $this->request->getHeader(Headers::HEADER_USER_AGENT);
        if ($userAgent == null || !$bots->existBot($userAgent)) {
            return false;
        }

        $isValidDomain = $this->isValidDomain($this->request->getDomain());
        if ($isValidDomain) {
            $this->isExternal = $this->areDifferent($this->request->getDomain(), $this->request->getHost());
            $this->log->debug('VALID: UserAgent valid: ' . $userAgent . ', Domain valid: ' . $this->request->getDomain() . ', Host: ' . $this->request->getHost() . ', resource: ' . ($this->isExternal ? 'External' : 'Internal'));
            return true;
        }
        $isValidHost = $this->isValidDomain($this->request->getHost());
        $this->isExternal = !$isValidHost;
        $this->log->debug('VALID: UserAgent valid: ' . $userAgent . ', Domain: ' . $this->request->getDomain() . ', Host: ' . $this->request->getHost() . ', resource: ' . ($this->isExternal ? 'External' : 'Internal'));
        return true;
    }
    public function isValidDomain($item)
    {
        return in_array($item, CDN_HOST);
    }
    private function areDifferent($item1, $item2)
    {
        return $item1 !== $item2;
    }
}
