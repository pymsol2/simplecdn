<?PHP

namespace Pymsol\SimpleCDN\Requests;

use Pymsol\Utilities\Url;
use Pymsol\SimpleCDN\Headers\Headers;
use Pymsol\SimpleCDN\Requests\RequestChecker;
use Pymsol\SimpleCDN\Exceptions\RequestUrlMissingException;

class Request
{
    private $scheme;
    private $origin;
    private $domain;
    private $host;
    private $path;
    private $request;
    private $queryString;
    private $referer;
    private $hostReferer;
    private $headers;

    private $acceptGzip;
    private $acceptWebp;
    private $ssl;

    private $isExternalResource;

    private const HTTP = 'http';
    private const HTTPS = 'https';

    public function __construct(array $request = null)
    {
        $this->headers = new Headers();
        if ($request == null) {
            return;
        }
        $this->setScheme($this->getServer($request, 'REQUEST_SCHEME'))
            ->setOrigin($this->getServer($request, 'HTTP_HOST'))
            ->setRequest($this->getServer($request, 'REDIRECT_QUERY_STRING'))
            ->setReferer($this->getServer($request, 'HTTP_REFERER'));

        $this->setAccept($this->getServer($request, 'HTTP_ACCEPT'))
            ->setAcceptEncoding($this->getServer($request, 'HTTP_ACCEPT_ENCODING'))
            ->setHeader(Headers::HEADER_CONNECTION, $this->getServer($request, 'HTTP_CONNECTION'))
            ->setHeader(Headers::HEADER_PRAGMA, $this->getServer($request, 'HTTP_PRAGMA'))
            ->setHeader(Headers::HEADER_CACHECONTROL, $this->getServer($request, 'HTTP_CACHE_CONTROL'))
            ->setHeader(Headers::HEADER_USER_AGENT, $this->getServer($request, 'HTTP_USER_AGENT'))
            ->setHeader(Headers::HEADER_IF_MODIFIED_SINCE, $this->getServer($request, 'HTTP_IF_MODIFIED_SINCE'));

        if ($request == '') {
            throw new RequestUrlMissingException();
        }

        $requestChecker = new RequestChecker();
        if (!$requestChecker->isValidDomain($this->domain)) {
            $this->domain = $this->host;
        }

        unset($requestChecker);
    }
    public function checkRequest($extension)
    {
        // security checks
        $requestChecker = new RequestChecker();
        $requestChecker->checks($this, $extension);
        $this->isExternalResource = $requestChecker->getIsExternal();
        unset($requestChecker);
    }
    public function getIsExternal()
    {
        return $this->isExternalResource;
    }
    private function getServer(array $request, string $name)
    {
        return isset($request[$name]) ? $request[$name] : null;
    }

    private function setScheme($value)
    {
        $this->scheme = $value;
        $this->ssl = strpos($value, self::HTTPS) !== false;
        return $this;
    }
    private function setOrigin($value)
    {
        $this->origin = $value;
        if (strpos($value, '.') === false) {
            $this->domain = $value;
            return $this;
        }
        list($subDomain, $domain) = explode('.', $value, 2);
        $this->domain = $domain;
        return $this;
    }
    /**
     *  $value = 'url=aptavs.com/prueba/esto.png&wer=ww&otro=34'
     */
    private function setRequest($value)
    {
        $this->request = $value;

        $values = explode('&', $value);
        $queryString = array();
        foreach ($values as $item) {
            $this->setQueryStringHostAndPath($queryString, $item);
        }
        $this->queryString = $queryString;
        return $this;
    }
    private function setQueryStringHostAndPath(array &$queryString, string $item)
    {
        if (strpos($item, 'url=') !== false) {
            $host = explode('=', $item)[1];
            $this->setHostAndPath($host);
            return;
        }
        $queryString[] = $item;
    }
    public function setHostAndPath(string $item)
    {
        if ($item == null) {
            return;
        }
        $parseUrl = (new Url())->parse($this->scheme . '://' . $item);
        $this->checkIsValidHost($parseUrl['host'], $parseUrl['path']);
    }
    private function checkIsValidHost($host, $path)
    {
        // CDN by folder -> http://mycdn.com/bob.com/resource...
        if (strpos($host, '.') !== false) {
            $this->host = $host;
            $this->path = $path;
            return;
        }

        // CDN by DNS like subdomain, not with folder -> http://mycdn.bob.com/resource...
        $this->host = $this->domain;
        $this->path = '/' . $host . $path;
    }
    private function setReferer($value)
    {
        $this->referer = $value;
        if ($value != null) {
            $parseUrl = (new Url())->parse($value);
            $this->hostReferer = $parseUrl['host'];
        }
        return $this;
    }
    public function getHeaders()
    {
        return $this->headers->getHeaders();
    }
    public function getHeader($key)
    {
        return $this->headers->getHeader($key);
    }
    private function setHeader(string $key, $value)
    {
        $this->headers->setHeader($key, $value);
        return $this;
    }
    private function setAccept($value)
    {
        $this->setHeader(Headers::HEADER_ACCEPT, $value);
        $this->acceptWebp = (strpos($value, Headers::HEADER_VALUE_WEBP) !== false);
        return $this;
    }
    private function setAcceptEncoding($value)
    {
        $this->setHeader(Headers::HEADER_ACCEPT_ENCODING, $value);
        $this->acceptGzip = strpos($value, Headers::HEADER_VALUE_GZIP) !== false;
        return $this;
    }
    public function getHost()
    {
        return $this->host;
    }
    public function getDomain()
    {
        return $this->domain;
    }
    public function getPath()
    {
        return $this->path;
    }
    public function getQueryString()
    {
        return $this->queryString;
    }
    public function getReferer()
    {
        return $this->referer;
    }
    public function getHostReferer()
    {
        return $this->hostReferer;
    }
    public function getAcceptGzip()
    {
        return $this->acceptGzip;
    }
    public function getSsl()
    {
        return $this->ssl;
    }
    public function setSsl(bool $value)
    {
        $this->ssl = $value ?? false;
        $this->scheme = $this->ssl ? self::HTTPS : self::HTTP;
        return $this;
    }
    public function getAcceptWebp()
    {
        return $this->acceptWebp;
    }

    public function getSchemeAndHost()
    {
        return $this->scheme . '://' . $this->host;
    }
    public function getUrl()
    {
        return $this->getSchemeAndHost() . $this->path;
    }

    public function getRequireAllowOrigin()
    {
        return strpos($this->referer, $this->host) !== false;
    }
}
