<?PHP

namespace Pymsol\SimpleCDN;

use Error;
use Exception;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\CdnFacade;

class Application
{
    public function __construct(array $request = null)
    {
        $log = (new Log())->getLogger(__CLASS__);
        try {
            $app = new CdnFacade();
            $app->prepare($request);
            $app->execute();
        } catch (Error | Exception $ex) {
            $log->error('Error: ' . $ex->getMessage());
        }
    }
}
