<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\SimpleLogger\Log;
use MatthiasMullie\Minify\JS;
use Pymsol\SimpleCDN\Resources\ResourceGzip;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceJs extends ResourceGzip
{
    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_JS;
    }

    public function saveFile($content)
    {
        // Optimize content
        $minifier = new JS();
        // plain JS
        $minifier->add($content);
        // output the content
        $content = $minifier->minify();
        parent::saveFile($content);
    }
}
