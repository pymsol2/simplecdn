<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Resources\ResourceBase;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceImageGD extends ResourceBase
{
    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_IMAGE;
    }

    public function saveFile($content)
    {
        $content = $this->isImageWithMeasurements($content);

        if (!$this->optimizeImage($content)) {
            parent::saveFile($content);
        }
        if ($this->extension != 'webp' && $this->acceptWebp) {
            $this->convertImageToWebP($content, COMPRESION_WEBP);
        }
    }

    private function convertImageToWebP($content, $quality = 85)
    {
        $image = imagecreatefromstring($content);
        imagepalettetotruecolor($image);
        imagealphablending($image, true);
        imagesavealpha($image, true);
        imagewebp($image, $this->file . self::SUFFIX_WEBP, $quality);
        imagedestroy($image);
    }

    private function optimizeImage($content)
    {
        if ($this->extension == 'heic' || $this->extension == 'gif') {
            return false;
        }

        $res = false;
        $image = imagecreatefromstring($content);
        $res = $this->saveImage($image, $this->file);
        imagedestroy($image);
        return $res;
    }

    private function isImageWithMeasurements($content)
    {
        $fileName = $this->file;
        $patern = '#^(?<name>.+)_(?<width>\d+)x(?<heigth>\d+)\.(?<extension>png|jpg|jpeg|ico|bmp|gif)$#i';
        preg_match_all($patern, $fileName, $matches);

        if (empty($matches['width'][0]) || empty($matches['heigth'][0])) {
            return $content;
        }
        // nuevas medidas
        $width = !empty($matches['width'][0]) ? $matches['width'][0] : '';
        $heigth = !empty($matches['heigth'][0]) ? $matches['heigth'][0] : '';

        $temp = $this->resize_image($content, $width, $heigth, true);

        $stream = fopen('php://memory', 'r+');
        $res = $this->saveImage($temp, $stream);
        rewind($stream);
        if ($res) {
            $content = stream_get_contents($stream);
        }
        return $content;
    }
    private function resize_image($content, $w, $h)
    {
        $image = imagecreatefromstring($content);
        $oldWidth = imagesx($image);
        $oldHeight = imagesy($image);
        $oldRatio = $oldWidth / $oldHeight;
        $newWidth = $w;
        $newHeight = $h;
        $newRatio = $w / $h;

        if ($newRatio > $oldRatio) {
            $newWidth = $w > $oldWidth ? $oldWidth : $newWidth;
            $newHeight = $newWidth * $h / $w;
            $dif = $oldHeight - ($oldWidth * $newHeight / $newWidth);
            $x1 = 0;
            $y1 = intval($dif / 2);
            $x2 = $oldWidth;
            $y2 = $oldHeight - $dif;
        } else {
            $newHeight = $w > $oldHeight ? $oldHeight : $newHeight;
            $newWidth = $newHeight * $w / $h;
            $dif = $oldWidth - ($oldHeight * $newWidth / $newHeight);
            $x1 = intval($dif / 2);
            $y1 = 0;
            $x2 = $oldWidth - $dif;
            $y2 = $oldHeight;
        }

        $temp = imagecreatetruecolor($newWidth, $newHeight);
        imagealphablending($temp, false);
        imagesavealpha($temp, true);

        imagecrop($image, array($x1, $y1, $x2, $y2));
        //imagecopyresized($temp, $image, 0, 0, $x1, $y1, $newWidth, $newHeight, $x2, $y2);
        imagecopyresampled($temp, $image, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);

        imagedestroy($image);
        return $temp;
    }
    private function resize_image2($content, $w, $h)
    {
        $image = imagecreatefromstring($content);
        $width = imagesx($image);
        $height = imagesy($image);

        //Obtengo el ratio horizontal y el vertical
        $wRatio = $width / $w;
        $hRatio = $height / $h;

        // Me quedo con el ratio menor, que es el que puedo utilizar
        $ratio = $wRatio > $hRatio ? $hRatio : $wRatio;

        // Obtengo los tamaños a usar
        $wScale = $w * $ratio;
        $hScale = $h * $ratio;

        // Obtengo las coordenadas a usar para que el recorte sea desde el centro de la imagen
        $startX = ($width - $wScale) / 2;
        $startY = ($height - $hScale) / 2;

        //$dst = imagecrop($image, ['x' => $startX, 'y' => $startY, 'width' =>  $wScale, 'height' => $hScale]);
        $temp = imagecreatetruecolor($w, $h);
        imagealphablending($temp, false);
        imagesavealpha($temp, true);
        imagecopyresized($temp, $image, 0, 0, $startX, $startY, $w, $h, $wScale, $hScale);
        imagedestroy($image);

        return $temp;
    }

    private function saveImage($imageOrResource, $file)
    {
        $res = false;
        if ($this->extension == 'jpeg' || $this->extension == 'jpg' || $this->extension == 'jpe') {
            $quality = COMPRESION_JPG;
            imagejpeg($imageOrResource, $file, $quality);
            $res = true;
        }
        if ($this->extension == 'png') {
            $quality = round(COMPRESION_PNG / 10);
            if ($quality < 0 || $quality > 9) {
                $quality = 9;
            }
            imagealphablending($imageOrResource, false);
            imagesavealpha($imageOrResource, true);
            imagepng($imageOrResource, $file, $quality);
            $res = true;
        }
        if ($this->extension == 'bmp') {
            imagebmp($imageOrResource, $file, true);
            $res = true;
        }
        if ($this->extension == 'gif') {
            imagegif($imageOrResource, $file);
            $res = true;
        }
        if ($this->extension == 'webp') {
            $quality = COMPRESION_WEBP;
            imagewebp($imageOrResource, $file, $quality);
            $res = true;
        }
        return $res;
    }

    // Muy interesante, el obtener el canal alpha y poder pegarlo en otra imagen o poder suavizarlo.
    // https://stackoverflow.com/questions/57992171/php-wrong-result-for-imagetruecolortopalette-with-png-with-transparency
    function extractAlpha($im)
    {

        // Ensure input image is truecolour, not palette
        if (!imageistruecolor($im)) {
            printf("DEBUG: Converting input image to truecolour\n");
            imagepalettetotruecolor($im);
        }

        // Get width and height
        $w = imagesx($im);
        $h = imagesy($im);

        // Allocate a new greyscale, palette (non-alpha!) image to hold the alpha layer, since it only needs to hold alpha values 0..127
        $alpha = imagecreate($w, $h);
        // Create a palette for 0..127
        for ($i = 0; $i < 128; $i++) {
            imagecolorallocate($alpha, $i, $i, $i);
        }

        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                // Get current color
                $rgba = imagecolorat($im, $x, $y);
                $a = ($rgba & 0x7F000000) >> 24;
                imagesetpixel($alpha, $x, $y, $a);
            }
        }
        return $alpha;
    }

    function applyAlpha($im, $alpha)
    {
        // If output image is truecolour
        //    iterate over pixels getting current color and just replacing alpha component
        // else (palettised)
        //    // find a transparent colour in the palette
        //    if not successful
        //       allocate transparent colour in palette
        //    iterate over pixels replacing transparent ones with allocated transparent colour

        // Get width and height
        $w = imagesx($im);
        $h = imagesy($im);

        // Ensure all the lovely new alpha we create will be saved when written to PNG
        imagealphablending($im, false);
        imagesavealpha($im, true);

        // If output image is truecolour, we can set alpha 0..127
        if (imageistruecolor($im)) {
            printf("DEBUG: Target image is truecolour\n");
            for ($x = 0; $x < $w; $x++) {
                for ($y = 0; $y < $h; $y++) {
                    // Get current color
                    $rgba = imagecolorat($im, $x, $y);
                    // Get alpha
                    $a = imagecolorat($alpha, $x, $y);
                    // printf("DEBUG: Setting alpha[%d,%d] = %d\n",$x,$y,$a);
                    $new = ($rgba & 0xffffff) | ($a << 24);
                    imagesetpixel($im, $x, $y, $new);
                }
            }
        } else {
            printf("DEBUG: Target image is palettised\n");
            // Must be palette image, get index of a fully transparent color
            $transp = -1;
            for ($index = 0; $index < imagecolorstotal($im); $index++) {
                $c = imagecolorsforindex($im, $index);
                if ($c["alpha"] == 127) {
                    $transp = $index;
                    printf("DEBUG: Found a transparent colour at index %d\n", $index);
                }
            }
            // If we didn't find a transparent colour in the palette, allocate one
            $transp = imagecolorallocatealpha($im, 0, 0, 0, 127);
            // Scan image replacing all pixels that are transparent in the original copied alpha channel with the index of a transparent pixel in current palette
            for ($x = 0; $x < $w; $x++) {
                for ($y = 0; $y < $h; $y++) {
                    // Essentially we are thresholding the alpha here. If it was more than 50% transparent in original it will become fully trasnparent now
                    $grey = imagecolorat($alpha, $x, $y) & 0xFF;
                    if ($grey > 64) {
                        //printf("DEBUG: Replacing transparency at %d,%d\n",$x,$y);
                        imagesetpixel($im, $x, $y, $transp);
                    }
                }
            }
        }
        return $im;
    }
}
