<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\SimpleLogger\Log;
use MatthiasMullie\Minify\CSS;
use Pymsol\SimpleCDN\Resources\ResourceGzip;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceCss extends ResourceGzip
{
    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_CSS;
    }

    public function saveFile($content)
    {
        // Optimize content
        $minifier = new CSS();
        // plain CSS
        $minifier->add($content);
        // output the content
        $content = $minifier->minify();
        //$content = $this->minify($content);

        parent::saveFile($content);
    }

    private function minify($content)
    {
        // (0.5 0.5 -0.5 [0.5 {0.5 to .
        $content = preg_replace('/([\( \s \- \[ \{])0./', '\1.', $content);

        //
        return $content;
    }
}
