<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Resources\ResourceBase;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceImageImagick extends ResourceBase
{
    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_IMAGE;
    }

    public function saveFile($content)
    {
        $content = $this->isImageWithMeasurements($content);
        $content = $this->optimizeImage($content);

        if ($this->extension != 'webp' && $this->extension != 'ico' && $this->extension != 'heic' && $this->extension != 'gif') {
            $im = new \imagick();
            if (in_array('WEBP', $im->queryFormats())) {
                $this->convertImageToWebP($content, COMPRESION_WEBP);
            } else {
                $this->convertImageToWebPGD($content, COMPRESION_WEBP);
            }
            unset($im);
        }
    }

    private function convertImageToWebP($content, $quality = 90)
    {
        $im = new \imagick();
        $im->readImageBlob($content);

        $im->setImageFormat('webp');
        $im->stripImage();
        $im->setImageCompressionQuality($quality);
        $im->setCompressionQuality($quality);

        // Image has transparency
        if (!$im->getImageAlphaChannel()) {
            // Remove alpha channel
            $im->setImageAlphaChannel(11);
            // Set image background color
            $im->setImageBackgroundColor('white');
            // Merge layers
            $im->mergeImageLayers(\imagick::LAYERMETHOD_FLATTEN);
        } else {
            $im->setImageAlphaChannel(\imagick::ALPHACHANNEL_ACTIVATE);
            $im->setBackgroundColor(new \ImagickPixel('transparent'));
        }

        $im->writeImage($this->file . self::SUFFIX_WEBP);
        $im->clear();
        $im->destroy();
    }
    private function convertImageToWebPGD($content, $quality = 90)
    {
        $image = imagecreatefromstring($content);
        imagepalettetotruecolor($image);
        imagealphablending($image, true);
        imagesavealpha($image, true);
        imagewebp($image, $this->file . self::SUFFIX_WEBP, $quality);
        imagedestroy($image);
    }

    private function optimizeImage($content)
    {
        $im = new \imagick();
        $im->readImageBlob($content);

        $im = $this->setType($im);

        $im->writeImage($this->file);
        $res = $im->getImageBlob();

        $im->clear();
        $im->destroy();
        return $res;
    }

    private function isImageWithMeasurements($content)
    {
        $fileName = $this->file;
        $patern = '#^(?<name>.+)_(?<width>\d+)x(?<heigth>\d+)\.(?<extension>png|jpg|jpeg|ico|bmp|gif)$#i';
        preg_match_all($patern, $fileName, $matches);

        if (empty($matches['width'][0]) || empty($matches['heigth'][0])) {
            return $content;
        }
        $width = !empty($matches['width'][0]) ? $matches['width'][0] : '';
        $heigth = !empty($matches['heigth'][0]) ? $matches['heigth'][0] : '';

        $im = new \imagick();
        $im->readImageBlob($content);

        $oldWidth = $im->getImageWidth();
        $oldHeight = $im->getImageHeight();
        $oldRatio = $oldWidth / $oldHeight;
        $newWidth = $width;
        $newHeight = $heigth;
        $newRatio = $width / $heigth;

        if ($newRatio > $oldRatio) {
            $newWidth = $width > $oldWidth ? $oldWidth : $newWidth;
            $newHeight = $newWidth * $heigth / $width;
            $dif = $oldHeight - ($oldWidth * $newHeight / $newWidth);
            $x1 = 0;
            $y1 = intval($dif / 2);
            $x2 = $oldWidth;
            $y2 = $oldHeight - $dif;
        } else {
            $newHeight = $width > $oldHeight ? $oldHeight : $newHeight;
            $newWidth = $newHeight * $width / $heigth;
            $dif = $oldWidth - ($oldHeight * $newWidth / $newHeight);
            $x1 = intval($dif / 2);
            $y1 = 0;
            $x2 = $oldWidth - $dif;
            $y2 = $oldHeight;
        }

        $im->cropImage($x2, $y2, $x1, $y1);
        $im->resizeImage($newWidth, $newHeight, \Imagick::FILTER_LANCZOS, 0.9);

        $content = $im->getImageBlob();
        $im->clear();
        $im->destroy();

        return $content;
    }

    private function setType($im)
    {
        $quality = 100;
        $profiles = $im->getImageProfiles("icc", true);
        $im->stripImage();
        if (!empty($profiles)) {
            $im->profileImage('icc', $profiles['icc']);
        }

        if ($this->extension == 'jpeg' || $this->extension == 'jpg' || $this->extension == 'jpe') {
            // Recomendado de google
            // convert image.jpg -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace RGB image_converted.jpg
            $im->setImageFormat('jpg');

            $im->setImageBackgroundColor('white');
            $im->mergeImageLayers(\imagick::LAYERMETHOD_FLATTEN);

            $im->setSamplingFactors(array('2x2', '1x1', '1x1'));

            $im->despeckleImage();
            $im->optimizeImageLayers();

            $im->setImageResolution(72, 72);
            $im->resampleImage(72, 72, \imagick::FILTER_UNDEFINED, 1);

            $im->setInterlaceScheme(\Imagick::INTERLACE_JPEG);
            $im->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $im->setCompression(\imagick::COMPRESSION_JPEG);

            $quality = COMPRESION_JPG;
        }
        if ($this->extension == 'png') {
            $im->setImageFormat('png');

            $quality = COMPRESION_PNG;
        }
        if ($this->extension == 'bmp') {
            $im->setImageFormat('bmp');

            $im->setImageBackgroundColor('white');
            $im->mergeImageLayers(\imagick::LAYERMETHOD_FLATTEN);

            $im->setSamplingFactors(array('2x2', '1x1', '1x1'));

            $im->despeckleImage();
            $im->optimizeImageLayers();

            $im->setImageResolution(72, 72);
            $im->resampleImage(72, 72, \imagick::FILTER_UNDEFINED, 1);
        }
        if ($this->extension == 'gif') {
            $im->setImageFormat('gif');

            $im->setImageBackgroundColor('white');
            $im->mergeImageLayers(\imagick::LAYERMETHOD_FLATTEN);

            $im->setSamplingFactors(array('2x2', '1x1', '1x1'));

            $im->despeckleImage();
            $im->optimizeImageLayers();

            $im->setImageResolution(72, 72);
            $im->resampleImage(72, 72, \imagick::FILTER_UNDEFINED, 1);
        }
        if ($this->extension == 'webp') {
            $im->setImageFormat('webp');

            $quality = COMPRESION_WEBP;
        }

        // Compresion
        $im->setImageCompressionQuality($quality);
        $im->setCompressionQuality($quality);

        // Image has transparency
        if (!$im->getImageAlphaChannel()) {
            // Remove alpha channel
            $im->setImageAlphaChannel(11);
            // Set image background color
            $im->setImageBackgroundColor('white');
            // Merge layers
            $im->mergeImageLayers(\imagick::LAYERMETHOD_FLATTEN);
        } else {
            $im->setImageAlphaChannel(\imagick::ALPHACHANNEL_ACTIVATE);
            $im->setBackgroundColor(new \ImagickPixel('transparent'));
        }

        return $im;
    }
}
