<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\SimpleLogger\Log;
use enshrined\svgSanitize\Sanitizer;
use Pymsol\SimpleCDN\Resources\ResourceGzip;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceSvg extends ResourceGzip
{
    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_SVG;
    }

    public function saveFile($content)
    {
        // Create a new sanitizer instance
        $sanitizer = new Sanitizer();
        // Pass it to the sanitizer and get it back clean
        $content = $sanitizer->sanitize($content);

        parent::saveFile($content);
    }
}
