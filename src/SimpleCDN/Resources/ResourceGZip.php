<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\Utilities\File;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Resources\ResourceBase;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceGzip extends ResourceBase
{
    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_GZIP;
    }

    public function saveFile($content)
    {
        parent::saveFile($content);
        $this->saveGzipFile($content);
    }
    public function saveGzipFile($content)
    {
        (new File())->save($this->file . self::SUFFIX_GZ, gzencode($content, 9, FORCE_GZIP), true);
    }
}
