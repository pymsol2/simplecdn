<?PHP

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\Utilities\File;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class ResourceBase
{
    protected $log;

    protected $file;
    protected $extension;
    protected $isNew;
    protected $type;
    protected $acceptWebp = false;

    public const SUFFIX_GZ = '.gz';
    public const SUFFIX_WEBP = '.webp';
    public const SUFFIX_HEADER = '.header';

    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->type = ResourceFactory::RESOURCE_BASE;
    }

    public function getFile()
    {
        return $this->file;
    }
    public function setFile(string $value)
    {
        if ($value == null) {
            return;
        }

        $this->file = $value;
        $extension = explode('.', $value);
        $this->extension = end($extension);

        $this->acceptWebp = ($this->extension != 'ico' && $this->extension != 'heic' && $this->extension != 'gif');
    }
    public function getExtension()
    {
        return $this->extension;
    }
    public function getIsNew()
    {
        return $this->isNew;
    }
    public function setIsNew(bool $value)
    {
        $this->isNew = $value;
    }
    public function getType()
    {
        return $this->type;
    }
    public function getAcceptWebp()
    {
        return $this->acceptWebp;
    }

    public function checkFileExist()
    {
        $this->isNew = true;
        if ((new File())->exist($this->file)) {
            $this->isNew = false;
        }
        return !$this->isNew;
    }

    public function getHeadersFromFile()
    {
        return (new File())->read($this->file . self::SUFFIX_HEADER);
    }
    public function saveHeaders(array $headers)
    {
        (new File())->save($this->file . self::SUFFIX_HEADER, json_encode($headers, JSON_PRETTY_PRINT), true);
    }
    public function saveFile($content)
    {
        (new File())->save($this->file, $content, true);
    }
}
