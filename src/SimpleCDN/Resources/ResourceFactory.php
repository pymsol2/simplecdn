<?php

namespace Pymsol\SimpleCDN\Resources;

use Pymsol\SimpleCDN\Resources\ResourceJs;
use Pymsol\SimpleCDN\Resources\ResourceCss;
use Pymsol\SimpleCDN\Resources\ResourceSvg;
use Pymsol\SimpleCDN\Resources\ResourceBase;
use Pymsol\SimpleCDN\Resources\ResourceGzip;
use Pymsol\SimpleCDN\Resources\ResourceImageGD;
use Pymsol\SimpleCDN\Resources\ResourceImageImagick;

class ResourceFactory
{
    public const RESOURCE_BASE = 'base';
    public const RESOURCE_GZIP = 'gzip';
    public const RESOURCE_CSS = 'css';
    public const RESOURCE_JS = 'js';
    public const RESOURCE_IMAGE = 'image';
    public const RESOURCE_SVG = 'svg';

    public const TYPES = array(
        'js' => array('text/javascript', 'application/javascript', 'application/x-javascript', 'text/ecmascript', 'application/ecmascript', 'text/jscript'),
        'css' => array('text/css'),
        'svg' => array('image/svg+xml'),
        'image' => array('image/'),
        'gzip' => array('application/xml', 'application/json', 'text/', 'application/ld+json', 'application/xhtml+xml', 'application/rss+xml', 'application/vnd.ms-fontobject', 'application/x-', 'font/'),
    );

    public static function getResourceInstance($type = null)
    {
        $res = null;
        switch ($type) {
            case self::RESOURCE_GZIP:
                $res = new ResourceGzip();
                break;
            case self::RESOURCE_CSS:
                $res = new ResourceCss();
                break;
            case self::RESOURCE_JS:
                $res = new ResourceJs();
                break;
            case self::RESOURCE_IMAGE:
                $res = self::getResourceInstanceImage();
                break;
            case self::RESOURCE_SVG:
                $res = new ResourceSvg();
                break;
            default:
                $res = new ResourceBase();
                break;
        }
        return $res;
    }
    public static function getResourceInstanceFromMimeType($type, $resource = null)
    {
        if ($type == null) {
            return $resource;
        }

        $extension = self::getExtension($type);
        $res = self::getResourceInstance($extension);
        if (get_class($res) === get_class($resource)) {
            return $resource;
        }

        $res->setFile($resource->getFile());
        $res->setIsNew($resource->getIsNew());
        return $res;
    }

    private static function getExtension($ContentType)
    {
        foreach (self::TYPES as $key => $values) {
            foreach ($values as $value) {
                if (strpos($ContentType, $value) !== false) {
                    return $key;
                }
            }
        }
        return null;
    }

    private static function getResourceInstanceImage()
    {
        $res = new ResourceBase();
        if (strcasecmp(EXTENSION_IMAGES, 'IMAGICK') == 0 && extension_loaded('imagick')) {
            $res = new ResourceImageImagick();
        } elseif (extension_loaded('gd')) {
            $res = new ResourceImageGD();
        }
        return $res;
    }
}
