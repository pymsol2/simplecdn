<?PHP

namespace Pymsol\SimpleCDN;

use Error;
use Pymsol\SimpleCDN\Cdn;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Requests\Request;

class CdnFacade
{
    private $log;
    private $cdn;

    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
    }

    public function prepare(array $request = null)
    {
        $this->cdn = new Cdn(new Request($request));
    }
    public function execute()
    {
        try {
            $this->cdn->send();
            die;
        } catch (Error $ex) {
            $this->log->error($ex->getMessage(), $ex);
        }
    }
}
