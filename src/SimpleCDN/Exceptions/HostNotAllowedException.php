<?php

namespace Pymsol\SimpleCDN\Exceptions;

use Pymsol\SimpleCDN\Exceptions\CdnException;

class HostNotAllowedException extends CdnException
{
}
