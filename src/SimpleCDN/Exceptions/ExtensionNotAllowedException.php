<?php

namespace Pymsol\SimpleCDN\Exceptions;

use Pymsol\SimpleCDN\Exceptions\CdnException;

class ExtensionNotAllowedException extends CdnException
{
}
