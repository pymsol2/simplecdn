<?PHP

namespace Pymsol\SimpleCDN\Responses;

use DateTime;
use DateTimeZone;
use Pymsol\Utilities\File;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Headers\Headers;

class ResponseHTTP
{
    private $log;

    private $response;

    public function __construct($response)
    {
        $this->log = (new Log())->getLogger(__CLASS__);

        $this->response = $response;
    }

    public function sendHeadersAndContent()
    {
        $this->cleanBufferAndHeaders();

        http_response_code(200);
        $expires = null;
        foreach ($this->response->getHeaders() as $key => $value) {
            switch ($key) {
                case Headers::HEADER_CONTENT_LENGTH:
                case 'date':
                case 'x-powered-by':
                case 'x-aspnet-version':
                case 'x-aspnetmvc-version':
                case 'server':
                case 'etag':
                    // Do noting
                    break;
                case Headers::HEADER_EXPIRE:
                    $expires = trim($value);
                    break;
                default:
                    header($key . ': ' . $value);
                    break;
            }
        }
        if ($expires == null) {
            $expires = $this->response->getExpires();
        }
        if ($expires != null) {
            header(Headers::HEADER_EXPIRE . ': ' . $expires);
        }

        $size = 0;
        $finalFile = $this->response->getFinalFile();
        if ($finalFile && (new File())->exist($finalFile)) {
            $size = (new File())->size($finalFile);
        }
        if ($size > 0) {
            header(Headers::HEADER_CONTENT_LENGTH . ': ' . $size);
        } else {
            $this->log->info('No file size for ' . $finalFile);
        }

        $this->sendHeaderDate();
        $this->sendHeaderRuntime();
        readfile($finalFile);
        $this->log->info('[200 - ' . ($this->response->getIsNew() ? 'New' : 'Old') . '], Request [' . $this->response->getUrl() . ']');
    }
    public function sendHeader302()
    {
        $this->cleanBufferAndHeaders();
        http_response_code(302);
        header('status: 302 Moved Permanently');
        header('location: ' . $this->response->getUrl());
        $this->sendHeaderRuntime();
        $this->log->info('[302 - Extension not allowed], Request [' . $this->response->getUrl() . ']');
    }
    public function sendHeader304()
    {
        $this->cleanBufferAndHeaders();
        http_response_code(304);
        header('status: 304 Not Modified');

        $this->sendHeader(Headers::HEADER_CACHECONTROL);
        $this->sendHeader(Headers::HEADER_CONTENT_LOCATION);
        $this->sendHeader(Headers::HEADER_LAST_MODIFIED);

        $this->sendHeaderDate();
        $this->sendHeaderRuntime();
        $this->log->info('[304 - Cache], Request [' . $this->response->getUrl() . ']');
    }
    private function sendHeader($key)
    {
        $value = $this->response->getHeader($key);
        if ($value != null) {
            header($key . ': ' . $value);
        }
    }
    public function sendHeader307()
    {
        $this->cleanBufferAndHeaders();
        http_response_code(307);
        header('status: 307 Temporary redirect');
        if ($this->response->getUrl() != null) {
            header('location: ' . $this->response->getUrl());
        }
        $this->sendHeaderDate();
        $this->sendHeaderRuntime();
        $this->log->info('[307 - ' . $this->response->getStatus() . '], Request [' . $this->response->getUrl() . ']');
    }
    public function sendHeader403()
    {
        $this->cleanBufferAndHeaders();
        http_response_code(403);
        $this->sendHeaderRuntime();
        $this->log->info('[403 - Host not allowed], Request [' . $this->response->getUrl() . ']');
    }

    private function cleanBufferAndHeaders()
    {
        ob_clean();
        if (!headers_sent()) {
            header_remove();
        }
    }

    private function getDateTimeNowFormatGMT($datetime)
    {
        $gmtTimezone = new DateTimeZone('GMT');
        $DateTimeGMT = new DateTime($datetime, $gmtTimezone);
        // Format: Mon, 17 Oct 2016 09:09:28 GMT
        return $DateTimeGMT->format('D, d M Y H:i:s e');
    }
    private function sendHeaderDate()
    {
        header('date: ' . $this->getDateTimeNowFormatGMT('now'));
    }
    private function sendHeaderRuntime()
    {
        header('x-powered-by: SimpleCDN v1.0.0');
        header('x-runtime: ' . round((microtime(true) - CDN_START) * 1000) . 'ms');
    }
}
