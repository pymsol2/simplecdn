<?PHP

namespace Pymsol\SimpleCDN\Responses;

use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Headers\Headers;
use Pymsol\SimpleCDN\Resources\ResourceBase;
use Pymsol\SimpleCDN\Resources\ResourceFactory;

class Response
{
    private $log;

    private $headers;
    private $expires;
    private $url;
    private $file;
    private $isNew;
    private $acceptGzip;
    private $acceptWebp;
    private $type;

    private $status;

    public function __construct($acceptGzip = null, $acceptWebp = null)
    {
        $this->log = (new Log())->getLogger(__CLASS__);

        $this->headers = new Headers();

        $this->acceptGzip = $acceptGzip ?? false;
        $this->acceptWebp = $acceptWebp ?? false;
        $this->status = 200;
    }

    public function getHeaders()
    {
        return $this->headers->getHeaders();
    }
    public function setHeadersFromRemote(string $value, $fileExtension)
    {
        $this->headers->setHeadersFromRemote($value, $fileExtension);
    }
    public function setHeadersFromJson(string $value, $fileExtension)
    {
        $this->headers->setHeadersFromJson($value, $fileExtension);

        $expires = $this->headers->getHeader(Headers::HEADER_EXPIRE);
        if ($expires != null) {
            $this->expires = $expires;
        }
    }
    public function setHeaders(Headers $headers)
    {
        $this->headers = $headers;
    }
    public function getHeader($key)
    {
        return $this->headers->getHeader($key);
    }
    public function setHeader($key, $value)
    {
        $this->headers->setHeader($key, $value);
    }

    public function getStatus()
    {
        return $this->status;
    }
    public function setStatus(int $value)
    {
        $this->status = $value;
    }

    public function getExpires()
    {
        return $this->expires;
    }
    public function setExpires($value)
    {
        $this->expires = $value;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function setUrl($value)
    {
        $this->url = $value;
    }
    public function setFile($value)
    {
        $this->file = $value;
    }
    public function getIsNew()
    {
        return $this->isNew;
    }
    public function setIsNew($value)
    {
        $this->isNew = $value;
    }
    public function getType()
    {
        return $this->type;
    }
    public function setType($value)
    {
        $this->type = $value;
    }

    public function send()
    {
        $responseHTTP = new ResponseHTTP($this);
        switch ($this->status) {
            case 403:
                $responseHTTP->sendHeader403();
                break;
            case 302:
                $responseHTTP->sendHeader302();
                break;
            case 304:
                $responseHTTP->sendHeader304();
                break;
            case 200:
                $responseHTTP->sendHeadersAndContent();
                break;
            default:
                $responseHTTP->sendHeader307();
                break;
        }
    }

    public function getFinalFile()
    {
        $res = $this->file;
        if (
            $this->acceptGzip && ($this->type == ResourceFactory::RESOURCE_GZIP
                || $this->type == ResourceFactory::RESOURCE_JS
                || $this->type == ResourceFactory::RESOURCE_CSS
                || $this->type == ResourceFactory::RESOURCE_SVG)
        ) {
            header(Headers::HEADER_CONTENT_ENCODING . ': ' . Headers::HEADER_VALUE_GZIP);
            $res .= ResourceBase::SUFFIX_GZ;
        }

        if ($this->type == ResourceFactory::RESOURCE_IMAGE && $this->acceptWebp) {
            header(Headers::HEADER_CONTENT_TYPE . ': ' . Headers::HEADER_VALUE_WEBP);
            $res = $this->file . ResourceBase::SUFFIX_WEBP;
        }
        return $res;
    }
}
