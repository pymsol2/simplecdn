<?PHP

namespace Pymsol\SimpleCDN\Headers;

use Error;
use DateTime;
use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Headers\Headers;

class HeadersValidator
{
    private $log;

    private $expires;

    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
    }

    public function validarCabeceras(Headers $headers, $fileExtension)
    {
        $this->expires = null;
        $newHeaders = array();
        foreach ($headers as $key => $value) {
            $this->setIfExpires($key, $value);
            $HeadersCacheControl =  $this->checkIfCacheControl($key, $value, $fileExtension);
            $this->mergeHeaders($newHeaders, $key, $value, $HeadersCacheControl);
        }
        if ($this->expires != null) {
            $newHeaders[Headers::HEADER_EXPIRE] = $this->expires;
        }

        if (
            !array_key_exists(Headers::HEADER_EXPIRE, $newHeaders) &&
            !array_key_exists(Headers::HEADER_CACHECONTROL, $newHeaders)
        ) {
            $newHeaders[Headers::HEADER_CACHECONTROL] = $this->setMyCacheControl($fileExtension);
        }

        return $newHeaders;
    }
    private function mergeHeaders(&$headers, $key, $value, $HeadersCacheControl)
    {

        if ($HeadersCacheControl != null) {
            $headers = $headers + $HeadersCacheControl;
            return;
        }
        if (!array_key_exists($key, $headers) && $value != null) {
            $headers[$key] = $value;
        }
    }

    private function setIfExpires($key, $value)
    {
        if ($key == Headers::HEADER_EXPIRE) {
            $this->expires = $value;
        }
    }
    private function checkIfCacheControl($key, $value, $fileExtension)
    {
        if ($key != Headers::HEADER_CACHECONTROL) {
            return null;
        }

        $newHeaders = array();
        $values = explode(',', $value);
        foreach ($values as $item) {
            if ($item == Headers::HEADER_PRIVATE || $item == Headers::HEADER_NO_CACHE) {
                $newHeaders[Headers::HEADER_CACHECONTROL] = $this->setMyCacheControl($fileExtension);
            }
            $pos = strpos($item, Headers::HEADER_CACHECONTROL_MAXAGE);
            if ($pos !== false) {
                $item = trim(substr($item, $pos + 8));
                //TODO: No se si esto es correcto
                if (strlen($item) > 10) {
                    $dateExpire = new DateTime($item);
                    $expire = $dateExpire->diff(new DateTime())->s;

                    $this->expires = $expire;
                }
            }
        }
        return $newHeaders;
    }
    private function setMyCacheControl($fileExtension)
    {
        $value = $this->getExpiresDefault($fileExtension);
        return ($value != null) ? Headers::HEADER_CACHECONTROL_MAXAGE . $value : null;
    }
    private function getExpiresDefault($fileExtension)
    {
        try {
            $value = array_key_exists($fileExtension, CDN_EXTENSIONS_WITH_EXPIRES) ? CDN_EXTENSIONS_WITH_EXPIRES[$fileExtension][Headers::HEADER_EXPIRE] : null;
        } catch (Error $ex) {
            $this->log->error('Array CDN_EXTENSIONS_WITH_EXPIRES in config.php incorrect, format: \'ext\' => array(\'Expires\' => 2592000),');
        }
        return $value;
    }
}
