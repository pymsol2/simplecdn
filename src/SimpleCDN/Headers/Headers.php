<?PHP

namespace Pymsol\SimpleCDN\Headers;

use Pymsol\SimpleLogger\Log;
use Pymsol\SimpleCDN\Headers\HeadersValidator;

class Headers
{
    private $log;

    private $headers;

    public const HEADER_EXPIRE = 'expires';
    public const HEADER_CACHECONTROL = 'cache-control';
    public const HEADER_PRAGMA = 'pragma';
    public const HEADER_CONNECTION = 'connection';

    public const HEADER_CACHECONTROL_MAXAGE = 'max-age=';
    public const HEADER_PRIVATE = 'private';
    public const HEADER_NO_CACHE = 'no-cache';
    public const HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = 'access-control-allow-origin';
    public const HEADER_USER_AGENT = 'user-agent';
    public const HEADER_CONTENT_ENCODING = 'content-encoding';
    public const HEADER_CONTENT_TYPE = 'content-type';
    public const HEADER_CONTENT_LOCATION = 'content-location';
    public const HEADER_CONTENT_LENGTH = 'content-length';
    public const HEADER_ACCEPT = 'accept';
    public const HEADER_ACCEPT_ENCODING = 'accept-encoding';

    public const HEADER_IF_MODIFIED_SINCE = 'if-modified-since';
    public const HEADER_LAST_MODIFIED = 'last-modified';
    public const HEADER_VALUE_WEBP = 'image/webp';
    public const HEADER_VALUE_GZIP = 'gzip';

    public function __construct()
    {
        $this->log = (new Log())->getLogger(__CLASS__);
        $this->headers = array();
    }

    public function getHeaders()
    {
        return $this->headers;
    }
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }
    public function setHeadersFromRemote(string $value, $fileExtension)
    {
        if ($value == null) {
            return;
        }

        $this->headers = array();
        $headers = explode("\n", trim($value));
        $headers = array_reduce($headers, function ($res, $item) {
            if (empty($item) || $item == "\r") {
                $res = array();
            } else {
                $res[] = $item;
            }
            return $res;
        }, array());

        //
        foreach ($headers as $header) {
            $items = explode(": ", $header, 2);
            $key = strtolower(trim($items[0]));
            if (count($items) == 2 && !in_array($key, HEADERS_NOT_ALLOWED)) {
                $this->headers[$key] = trim($items[1]);
            }
        }
        $this->validarCabeceras($fileExtension);
    }
    public function setHeadersFromJson(string $value, $fileExtension)
    {
        if ($value == null) {
            return;
        }

        $this->headers = array();
        $headers = json_decode($value, true);
        foreach ($headers as $key => $value) {
            $this->headers[strtolower(trim($key))] = trim($value);
        }

        $this->validarCabeceras($fileExtension);
    }
    public function getHeader($name)
    {
        if ($name == null) {
            return null;
        }
        return array_key_exists($name, $this->headers) ? $this->headers[$name] : null;
    }
    public function setHeader(string $key, $item)
    {
        if ($key == null || $item == null) {
            return;
        }
        $this->headers[$key] = $item;
    }

    private function validarCabeceras($fileExtension)
    {
        $headerValidator = new HeadersValidator();
        $cache = $headerValidator->validarCabeceras($this, $fileExtension);

        $this->headers = array_merge($this->headers, $cache);
    }
}
