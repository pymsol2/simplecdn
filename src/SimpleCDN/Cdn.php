<?php

namespace Pymsol\SimpleCDN;

use DateTime;
use Pymsol\Utilities\Curl;
use Pymsol\SimpleLogger\Log;
use Pymsol\Utilities\MimeTypes;
use Pymsol\SimpleCDN\Headers\Headers;
use Pymsol\SimpleCDN\Requests\Request;
use Pymsol\SimpleCDN\Responses\Response;
use Pymsol\SimpleCDN\Exceptions\CdnException;
use Pymsol\SimpleCDN\Resources\ResourceFactory;
use Pymsol\SimpleCDN\Exceptions\HostNotAllowedException;
use Pymsol\SimpleCDN\Exceptions\RequestUrlMissingException;
use Pymsol\SimpleCDN\Exceptions\ExtensionNotAllowedException;

class Cdn
{
    private $log;

    private $request;
    private $resource;
    private $response;

    private $isExternalResource;

    private const CONTENT_PATH = 'content';
    private const CONTENT_EXTERNAL_PATH = 'contentExternal';

    /**
     * "Start" the CDN:
     */
    public function __construct(Request $request)
    {
        $this->log = (new Log())->getLogger(__CLASS__);

        try {
            $this->initParams($request);

            // security checks
            $this->request->checkRequest($this->resource->getExtension());
            $this->setIsExternalResource($this->request->getIsExternal());

            // check file
            $this->checkFile();
        } catch (ExtensionNotAllowedException $ex) {
            $this->log->error($ex->getMessage());
            $this->response->setStatus(302);
        } catch (RequestUrlMissingException | HostNotAllowedException | CdnException $ex) {
            $this->log->error($ex->getMessage());
            $this->response->setStatus(403);
        }
    }
    private function initParams(Request $request)
    {
        $this->request = $request;
        $this->resource = ResourceFactory::getResourceInstance();
        $this->resource->setFile($this->getFileName());
        $this->response = new Response($this->request->getAcceptGzip(), ($request->getAcceptWebp() && $this->resource->getAcceptWebp()));
    }
    private function setIsExternalResource($value)
    {
        $this->isExternalResource = $value;
        $this->resource->setFile($this->getFileName());
    }

    private function getFileName()
    {
        $rutaContent = (($this->isExternalResource) ? self::CONTENT_EXTERNAL_PATH  . DS . $this->request->getDomain() : self::CONTENT_PATH);
        return ROOT . $rutaContent . DS . $this->request->getHost() . $this->request->getPath();
    }
    private function checkFile()
    {
        // Check if exist file
        if ($this->resource->checkFileExist()) {
            return $this->getHeadersFromFile();
        }
        return $this->getUrlFile();
    }

    private function getHeadersFromFile()
    {
        $this->response->setHeadersFromJson($this->resource->getHeadersFromFile(), $this->resource->getExtension());

        // check if it is expired
        $now = new DateTime();
        if ($this->response->getExpires() != null && $now > $this->response->getExpires()) {
            $this->log->info('El recurso ha expirado [Expires:' . $this->response->getExpires() . '], lo pedimos al origen [' . $this->request->getUrl() . ']');
            $this->getUrlFile();
            return;
        }

        $this->setRightResource();
        $this->setAccessControlAllowOriginIfRequired();
    }
    private function getUrlFile()
    {
        if ($this->isImageWithMeasurements()) {
            return;
        }

        $this->getUrl();
    }

    private function getUrl()
    {
        //TODO: hay que ver el tema del host, si lo mandamos otros dominios nos pueden prohibir
        $params = array(
            'url' => $this->request->getUrl(),
            'method' => 'GET',
            'userAgent' => $this->request->getHeader('user-agent'),
            'headers' => $this->request->getHeaders(),
        );
        $queryString = $this->request->getQueryString();
        if ($queryString !== '') {
            $params['fields'] = $queryString;
        }

        // TODO: https://stackoverflow.com/questions/38417350/php-curl-realtime-proxy-stream-file
        // https://www.php.net/manual/es/function.readfile.php
        $res = (new Curl())->getUrl($params);

        if ($this->isErrorResponse($res)) {
            return null;
        }

        $this->response->setHeadersFromRemote($res['header'], $this->resource->getExtension());
        $this->response->setHeader('link', '<' . $this->request->getUrl() . '>; rel="canonical"');
        $this->setRightResource();
        $this->setAccessControlAllowOriginIfRequired();

        $this->resource->saveHeaders($this->response->getHeaders());
        $this->resource->saveFile($res['body']);
        return $res['body'];
    }

    private function isErrorResponse($curlResponse)
    {
        $res = false;
        if (!empty($curlResponse['curlError'])) {
            return $this->isCurlError($curlResponse['curlError']);
        }
        if ($curlResponse['httpCode'] !== 200) {
            if ($this->isImageWithMeasurements()) {
                return true;
            }
            throw new CdnException('Al descargar el fichero [' . $this->request->getUrl() . '] (Status:' . $curlResponse['httpCode'] . ')');
        }
        return $res;
    }
    private function isCurlError($curlError)
    {
        if ($this->request->getSsl()) {
            $this->request->setSsl(false);
            $this->getUrl();
            return true;
        }
        throw new CdnException('Al descargar el fichero [' . $this->request->getUrl() . '] - (' . $curlError . ')');
    }

    private function isImageWithMeasurements()
    {
        $fileName = $this->request->getHost() . $this->request->getPath();
        $patern = '#^(?<name>.+)_(?<width>\d+)x(?<heigth>\d+)\.(?<extension>png|jpg|jpeg|ico|bmp|gif)$#i';
        preg_match_all($patern, $fileName, $matches);

        if (empty($matches['width'][0]) || empty($matches['heigth'][0])) {
            return false;
        }
        $file = !empty($matches['name'][0]) ? $matches['name'][0] : '';
        $width = !empty($matches['width'][0]) ? $matches['width'][0] : '';
        $heigth = !empty($matches['heigth'][0]) ? $matches['heigth'][0] : '';
        $extension = !empty(['extension'][0]) ? '.' . $matches['extension'][0] : '';

        $this->request->setHostAndPath($file . $extension);
        $this->resource->setFile($this->getFileName());

        $this->log->info('Pedimos el fichero sin medidas [Original:' . $fileName . ']');
        $content = $this->getUrl();

        if ($content == null) {
            return false;
        }

        $this->request->setHostAndPath($file . '_' . $width . 'x' . $heigth . $extension);
        $this->resource->setFile($this->getFileName());
        $this->resource->saveHeaders($this->response->getHeaders());
        $this->resource->saveFile($content);
        return true;
    }

    private function setRightResource()
    {
        $mimes = new MimeTypes();

        $ContentType = $this->response->getHeader(Headers::HEADER_CONTENT_TYPE);
        if ($ContentType == null) {
            $this->log->error('El servidor remoto [' . $this->request->getHost() . '] no tiene configurado un mimetype para los ficheros ' . $this->resource->getExtension());

            $ContentType = $mimes->getMimeType($this->resource->getExtension());
        }

        if ($ContentType == null) {
            $this->log->error('Nos falta el mimetype para la extension ' . $this->resource->getExtension() . ', en el fichero MimeTypes.php');
            return;
        }

        $this->response->setHeader(Headers::HEADER_CONTENT_TYPE, $ContentType);
        $this->resource = ResourceFactory::getResourceInstanceFromMimeType($ContentType, $this->resource);
    }

    private function setAccessControlAllowOriginIfRequired()
    {
        $value = $this->response->getHeader(Headers::HEADER_ACCESS_CONTROL_ALLOW_ORIGIN);
        if ($this->request->getRequireAllowOrigin() && $value == null) {
            $this->response->setHeader(Headers::HEADER_ACCESS_CONTROL_ALLOW_ORIGIN, $this->request->getSchemeAndHost());
        }
    }

    public function send()
    {
        $this->response->setUrl($this->request->getUrl());
        $this->response->setFile($this->resource->getFile());
        $this->response->setIsNew($this->resource->getIsNew());
        $this->response->setType($this->resource->getType());

        $this->sendAllowed();
        $this->response->send();
    }
    private function sendAllowed()
    {
        if ($this->isLater()) {
            $this->response->setStatus(304);
            return true;
        }
        if ($this->response->getStatus() < 400) {
            $this->response->setStatus(200);
            return true;
        }
        $this->response->setStatus(307);
    }
    private function isLater()
    {
        $res = false;
        $dateIfModifiedSince = $this->request->getHeader(Headers::HEADER_IF_MODIFIED_SINCE);
        $dateLastModified = $this->response->getHeader(Headers::HEADER_LAST_MODIFIED);
        if ($dateLastModified != null && $dateIfModifiedSince != null && $dateIfModifiedSince >= $dateLastModified) {
            $res = true;
        }
        return $res;
    }
}
