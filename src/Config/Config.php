<?php

/**
 * Configuration
 *
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 */
define('DS', DIRECTORY_SEPARATOR);
define('CDN_START', microtime(true));
define('ROOT', dirname(APP) . DS);

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/
if (file_exists(ROOT . 'vendor/autoload.php')) {
    require ROOT . 'vendor/autoload.php';
}

/**
 * Configuration for: Error reporting
 * Useful to show every little problem during development, but only show hard errors in production
 */
define('ENVIRONMENT', 'development');

if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
} else {
    error_reporting(E_ALL);
    ini_set("display_errors", 0);
}

/**
 * Lebreria de logger, LOG4PHP o FAKE
 */
define('LOGGER', 'LOG4PHP');

/**
 * Compresion jpg format, recommended 85
 * Compresion png format, recommended 90/95
 *     Plus compression numbers divisible by 5
 */
define('COMPRESION_JPG', 85);
define('COMPRESION_PNG', 95);
define('COMPRESION_WEBP', 90);

/**
 * Configuration for resource image
 *   IMAGICK / GD
 */
define('EXTENSION_IMAGES', 'IMAGICK');

define('HEADERS_NOT_ALLOWED', array(
    'access-control-allow-origin',
    'content-length',
    'etag',
    'fastly-debug-digest',
    'server',
    'strict-transport-security',
    'vary',
    'x-aspnet-version',
    'x-aspnetmvc-version',
    'x-backend-server',
    'x-cache',
    'x-cache-hits',
    'x-content-type-options',
    'x-frame-options',
    'x-powered-by',
    'x-served-by',
    'x-ua-compatible',
    'x-xss-protection',
    'via'
));

define('CDN_HOST', array(
    'revista-fitness.com',
    'megazona.com',
    'aptavs.com',
    'biografiaspym.com',
    'camarasip.com',
    'tododoping.com',
    'feepyf.com',
    'zonavirus.com',
    'foros.zonavirus.com',
    'diversaservicios.es',
    'hidrolux.com',
    'biometrics-on.com',
    'zonavirusa.com',
    'escuelamakeup.com',
    'static.escuelamakeup.com',
    'i.vimeocdn.com'
));

define('CDN_EXTENSIONS_WITH_EXPIRES', array(
    'js' => array('expires' => 2592000),
    'jpeg' => array('expires' => 2592000),
    'css' => array('expires' => 2592000),
    'jpg' => array('expires' => 2592000),
    'png' => array('expires' => 2592000),
    'gif' => array('expires' => 2592000),
    'pdf' => array('expires' => 2592000),
    'ogv' => array('expires' => 2592000),
    'svg' => array('expires' => 2592000),
    'eot' => array('expires' => 2592000),
    'woff' => array('expires' => 2592000),
    'woff2' => array('expires' => 2592000),
    'ttf' => array('expires' => 8640000),
    'doc' => array('expires' => 2592000),
    'mp4' => array('expires' => 2592000),
    'webm' => array('expires' => 2592000),
    'ico' => array('expires' => 8640000),
    'jfif' => array('expires' => 8640000),
    'heic' => array('expires' => 8640000)
));
