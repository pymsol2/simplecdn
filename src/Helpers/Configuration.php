<?php

namespace Pymsol\Helpers;

use Pymsol\Utilities\File;

class Configuration
{

    protected $data;

    public function load(string $file)
    {
        // parse_ini_file
        // https://translate.google.com/translate?hl=es&sl=en&tl=es&u=https%3A%2F%2Fdocs.php.earth%2Fsecurity%2Fconfiguration%2F

        // https://translate.googleusercontent.com/translate_c?depth=1&hl=es&pto=aue&rurl=translate.google.com&sl=en&sp=nmt4&tl=es&u=https://github.com/vlucas/phpdotenv&usg=ALkJrhhsNi7OzwMgaqyH6Y1dcsios97ZZw
        // https://github.com/hassankhan/config
        $content = (new File())->read($file);
        $json = json_decode($content);
        $this->data = $json;
    }

    public function gatData()
    {
        return $this->data;
    }
}
