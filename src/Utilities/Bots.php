<?PHP

namespace Pymsol\Utilities;

class Bots
{
    private const BOTS = array(
        array('Nombre' => 'YahooSeeker [Bot]', 'User_Agent' => 'YahooSeeker/'),
        array('Nombre' => 'Yahoo [Bot]', 'User_Agent' => 'Yahoo! Slurp'),
        array('Nombre' => 'Yahoo Slurp [Bot]', 'User_Agent' => 'Yahoo! DE Slurp'),
        array('Nombre' => 'Yahoo MMCrawler [Bot]', 'User_Agent' => 'Yahoo-MMCrawler/'),
        array('Nombre' => 'YaCy [Bot]', 'User_Agent' => 'yacybot'),
        array('Nombre' => 'Xenu Link Sleuth', 'User_Agent' => 'Xenu Link Sleuth/'),
        array('Nombre' => 'W3C [Validator]', 'User_Agent' => 'W3C_Validator'),
        array('Nombre' => 'W3C [Linkcheck]', 'User_Agent' => 'W3C-checklink/'),
        array('Nombre' => 'W3 [Sitesearch]', 'User_Agent' => 'W3 SiteSearch Crawler'),
        array('Nombre' => 'Voyager [Bot]', 'User_Agent' => 'voyager/'),
        array('Nombre' => 'Uptime [Bot]', 'User_Agent' => 'Uptimebot/'),
        array('Nombre' => 'TurnitinBot [Bot]', 'User_Agent' => 'TurnitinBot/'),
        array('Nombre' => 'Telekom [Bot]', 'User_Agent' => 'crawleradmin.t-info@telekom.de'),
        array('Nombre' => 'Steeler [Crawler]', 'User_Agent' => 'http://www.tkl.iis.u-tokyo.ac.jp/~crawler/'),
        array('Nombre' => 'Snappy [Bot]', 'User_Agent' => 'Snappy/1.1 ( http://www.urltrends.com/ )'),
        array('Nombre' => 'SEOSearch [Crawler]', 'User_Agent' => 'SEOsearch/'),
        array('Nombre' => 'Seoma [Crawler]', 'User_Agent' => 'Seoma [SEO Crawler]'),
        array('Nombre' => 'SEO Crawler', 'User_Agent' => 'SEO search Crawler/'),
        array('Nombre' => 'Sensis [Crawler]', 'User_Agent' => 'Sensis Web Crawler'),
        array('Nombre' => 'psbot [Picsearch]', 'User_Agent' => 'psbot/0'),
        array('Nombre' => 'Online link [Validator]', 'User_Agent' => 'online link validator'),
        array('Nombre' => 'oBot [Crawler]', 'User_Agent' => 'http://filterdb.iss.net/crawler/'),
        array('Nombre' => 'Nutch [Bot]', 'User_Agent' => 'http://lucene.apache.org/nutch/'),
        array('Nombre' => 'MSNbot Media', 'User_Agent' => 'msnbot-media/'),
        array('Nombre' => 'MSN [Bot]', 'User_Agent' => 'msnbot/'),
        array('Nombre' => 'MSN NewsBlogs', 'User_Agent' => 'msnbot-NewsBlogs/'),
        array('Nombre' => 'Metager [Bot]', 'User_Agent' => 'MetagerBot/'),
        array('Nombre' => 'Majestic-12 [Bot]', 'User_Agent' => 'MJ12bot/'),
        array('Nombre' => 'ichiro [Crawler]', 'User_Agent' => 'ichiro/'),
        array('Nombre' => 'ICCrawler - ICjobs', 'User_Agent' => 'ICCrawler - ICjobs'),
        array('Nombre' => 'IBM Research [Bot]', 'User_Agent' => 'ibm.com/cs/crawler'),
        array('Nombre' => 'Heritrix [Crawler]', 'User_Agent' => 'heritrix/1.'),
        array('Nombre' => 'Heise IT-Markt [Crawler]', 'User_Agent' => 'heise-IT-Markt-Crawler'),
        array('Nombre' => 'Google [Bot]', 'User_Agent' => 'Googlebot'),
        array('Nombre' => 'Google Feedfetcher', 'User_Agent' => 'Feedfetcher-Google'),
        array('Nombre' => 'Google Desktop', 'User_Agent' => 'Google Desktop'),
        array('Nombre' => 'Google Adsense [Bot]', 'User_Agent' => 'Mediapartners-Google'),
        array('Nombre' => 'Gigabot [Bot]', 'User_Agent' => 'Gigabot/'),
        array('Nombre' => 'Francis [Bot]', 'User_Agent' => 'http://www.neomo.de/'),
        array('Nombre' => 'FAST WebCrawler [Crawler]', 'User_Agent' => 'FAST-WebCrawler/'),
        array('Nombre' => 'FAST Enterprise [Crawler]', 'User_Agent' => 'FAST Enterprise Crawler'),
        array('Nombre' => 'Facebook [Link]', 'User_Agent' => 'facebookexternalhit/'),
        array('Nombre' => 'Facebook [Bot]', 'User_Agent' => 'Facebot'),
        array('Nombre' => 'Exabot [Bot]', 'User_Agent' => 'Exabot'),
        array('Nombre' => 'Bing [Bot]', 'User_Agent' => 'bingbot/'),
        array('Nombre' => 'Baidu [Spider]', 'User_Agent' => 'Baiduspider'),
        array('Nombre' => 'Ask Jeeves [Bot]', 'User_Agent' => 'Ask Jeeves'),
        array('Nombre' => 'Alta Vista [Bot]', 'User_Agent' => 'Scooter/'),
        array('Nombre' => 'Alexa [Bot]', 'User_Agent' => 'ia_archiver'),
        array('Nombre' => 'AdsBot [Google]', 'User_Agent' => 'AdsBot-Google')
    );

    private function findStringInArray($content)
    {
        return array_filter(self::BOTS, function ($value) use ($content) {
            return strpos($content, $value['User_Agent']) !== false;
        });
    }

    public function existBot(string $userAgent)
    {
        $res = true;
        $results = $this->findStringInArray($userAgent);
        if (empty($results)) {
            $res = false;
        }
        return $res;
    }
}
