<?PHP

namespace Pymsol\Utilities;

class Curl
{
    private const TIMEOUT_DEFAULT = 20;
    private const USERAGENT_DEFAULT = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.8.0.9) Gecko/20061206 Firefox/1.5.0.9';

    /**
     * Send a requst using cURL
     *
     * $params = array('url' => '', 'method' => '', 'fields' => '', 'userAgent' => '', 'host' => '',
     *      'headers' => array(), 'referer' => '', 'cookie' => '', 'login' => '', 'password' => '', 'timeout' => 0);
     *
     * return array('header' => '', 'body' => '', 'curlError' => '', 'httpCode' => '', 'lastUrl' => '');
     **/
    public function getUrl(array $params)
    {
        $ch = $this->initParams($params);
        return $this->send($ch);
    }

    private function initParams(array $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTREDIR, 5);

        if (isset($params['referer']) && $params['referer'] && !empty($params['referer'])) {
            curl_setopt($ch, CURLOPT_REFERER, $params['referer']);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_USERAGENT, $this->getUserAgent($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders($params));

        $url = $params['url'];
        if ($params['method'] == "HEAD") {
            curl_setopt($ch, CURLOPT_NOBODY, 1);
        }
        if ($params['method'] == "POST") {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params['fields']));
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        }
        if ($params['method'] == "GET") {
            $url .= (strpos($url, '?') === false ? '?' : '') . http_build_query($params['fields']);
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        if (isset($params['cookie']) && $params['cookie']) {
            curl_setopt($ch, CURLOPT_COOKIE, $params['cookie']);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (isset($params['login']) & isset($params['password'])) {
            curl_setopt($ch, CURLOPT_USERPWD, $params['login'] . ':' . $params['password']);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->getTimeOut($params));

        return $ch;
    }
    /**
     * return array(
     *        'header' => '',
     *        'body' => '',
     *        'curlError' => '',
     *        'httpCode' => '',
     *        'lastUrl' => ''
     *    );
     *
     **/
    private function send($ch)
    {
        $response = curl_exec($ch);
        $error = curl_error($ch);
        $result = array(
            'header' => '',
            'body' => '',
            'curlError' => '',
            'httpCode' => '',
            'lastUrl' => '',
        );

        if ($error != "") {
            $result['curlError'] = $error;
            return $result;
        }

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $result['header'] = substr($response, 0, $header_size);
        $result['body'] = substr($response, $header_size);
        $result['httpCode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result['lastUrl'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

        curl_close($ch);
        return $result;
    }

    private function getUserAgent(array $params)
    {
        return $params['userAgent'] ?? self::USERAGENT_DEFAULT;
    }
    private function getHeaders(array $params)
    {
        $header = array();
        if (isset($params['host']) && $params['host'] && !empty($params['host'])) {
            $header[] = "Host: " . $params['host'];
        }
        if (isset($params['headers']) && $params['headers']) {
            $header = $header + $params['headers'];
        }
        return $header;
    }
    private function getTimeOut(array $params)
    {
        return $params['timeout'] ?? self::TIMEOUT_DEFAULT;
    }

    /**
     *  Fire and forget - Launch the request and not wait for its response, we can use it to launch requests
     *     that have to be executed without waiting for its response
     *     (processes that can be slow, compress images, compact css and js)
     *
     *  http://blog.markturansky.com/archives/205
     */
    public function sendPostAndClose($url, $params = array())
    {
        // create POST string
        $post_params = array();
        foreach ($params as $key => &$val) {
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        // get URL segments
        $parts = parse_url($url);

        // workout port and open socket
        $port = isset($parts['port']) ? $parts['port'] : 80;
        $fp = fsockopen($parts['host'], $port, $errno, $errstr, 30);

        // create output string
        $output = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $output .= "Host: " . $parts['host'] . "\r\n";
        $output .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $output .= "Content-Length: " . strlen($post_string) . "\r\n";
        $output .= "Connection: Close\r\n\r\n";
        $output .= isset($post_string) ? $post_string : '';

        // send output to $url handle
        fwrite($fp, $output);
        fclose($fp);
    }
}
