<?PHP

namespace Pymsol\Utilities;

class Url
{
    /**
     * return array('scheme' => '',
     *              'host' => '',
     *              'port' => '',
     *              'user' => '',
     *              'pass' => '',
     *              'path' => '',
     *              'query' => '',
     *              'fragment' => ''
     * )
     */
    public function parse($url)
    {
        if ($url == null) {
            return;
        }
        return parse_url($url);
    }

    /**
     * return 'http://username:password@hostname:9090/path?arg=value#anchor'
     */
    public function unparse(array $parseUrl)
    {
        $scheme = isset($parseUrl['scheme']) ? $parseUrl['scheme'] . '://' : '';
        $user = isset($parseUrl['user']) ? $parseUrl['user'] : '';
        $pass = isset($parseUrl['pass']) ? ':' . $parseUrl['pass'] : '';
        $pass = ($user || $pass) ? $pass . '@' : '';
        $host = isset($parseUrl['host']) ? $parseUrl['host'] : '';
        $port = isset($parseUrl['port']) ? ':' . $parseUrl['port'] : '';
        $path = isset($parseUrl['path']) ? $parseUrl['path'] : '';
        $query = isset($parseUrl['query']) ? '?' . $parseUrl['query'] : '';
        $fragment = isset($parseUrl['fragment']) ? '#' . $parseUrl['fragment'] : '';

        return $scheme . $user . $pass . $host . $port . $path . $query . $fragment;
    }

    /**
     * URL-encoded string. ex: change '%20'->' '
     * Decodes any %## encoding in the given string. Plus symbols ('+') are decoded to a space character.
     *
     * return 'http://username:password@hostname:9090/path?arg=value#anchor'
     */
    public function decode(string $url)
    {
        return urldecode($this->parse($url));
    }

    /**
     * URL-encodes string. ex: change ' '->'%20'
     *
     * return 'http://username:password@hostname:9090/path?arg=value#anchor'
     */
    public function encode(string $url)
    {
        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $replacements = array('!', '*', '\'', '(', ')', ';', ':', '@', '&', '=', '+', '$', ',', '/', '?', '%', '#', '[', ']');
        return str_replace($entities, $replacements, urlencode($url));
    }
}
