<?PHP

namespace Pymsol\Utilities;

class File
{
    public function exist($file)
    {
        return file_exists($file);
    }
    public function makeDirectory($path, $mode = 0755, $recursive = false)
    {
        return mkdir($path, $mode, $recursive);
    }

    public function read($file)
    {
        return file_get_contents($file);
    }
    public function save($file, $data, $force = false)
    {
        if ($force) {
            $this->createFolders($file);
        }
        file_put_contents($file, $data);
    }
    public function createFolders($file)
    {
        $file = str_replace(ROOT, '', $file);
        $folders = explode('/', $file, -1);
        if ($folders !== false) {
            $exist = true;
            $item = ROOT;
            foreach ($folders as $folder) {
                $item .= DS . $folder;
                if (!$this->exist($item)) {
                    $exist = false;
                }
            }

            if (!$exist) {
                $this->makeDirectory($item, 0755, true);
            }
        }
    }

    public function move($file, $target)
    {
        return rename($file, $target);
    }
    public function copy($file, $target)
    {
        return copy($file, $target);
    }
    public function chmod($file, $mode = null)
    {
        if ($mode) {
            return chmod($file, $mode);
        }
    }

    public function delete($file)
    {
        $path = realpath($file);

        if (isDir($file)) {
            return $this->deleteRecursivo($file);
        }
        if (is_file($file)) {
            return @unlink($file);
        }
    }
    private function deleteRecursivo($src)
    {
        $dir = opendir($src);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    deleteRecursivo($full);
                }
                if (isFile($full)) {
                    @unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }

    public function size($file)
    {
        return filesize($file);
    }
    public function modificationTime($file)
    {
        return filemtime($file);
    }

    // fifo, char, dir, block, link, file, socket y unknown.
    public function type($file)
    {
        return filetype($file);
    }

    public function isDir($file)
    {
        return is_dir($file);
    }
    public function isFile($file)
    {
        return is_file($file);
    }

    /**
     * echo $pathInfo['dirname'], "\n";
     * echo $pathInfo['basename'], "\n";
     * echo $pathInfo['extension'], "\n";
     * echo $pathInfo['filename'], "\n";
     */
    public function pathInfo($file)
    {
        return pathinfo($file);
    }

    // Returns canonicalized absolute pathname
    public function realPath($file)
    {
        return realpath($file);
    }

    public function getMimeType($file)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $res = finfo_file($finfo, $file);
        finfo_close($finfo);
        return $res;
    }

    public function hash($file)
    {
        return md5_file($file);
    }
}
