<?PHP

namespace Pymsol\Utilities;

class MimeTypes
{
    private const MIMES_CDN = array(
        array('type' => 'application/javascript', 'extension' => 'js'),
        array('type' => 'text/javascript', 'extension' => 'js'),
        array('type' => 'application/x-javascript', 'extension' => 'js'),
        array('type' => 'text/ecmascript', 'extension' => 'js'),
        array('type' => 'application/ecmascript', 'extension' => 'js'),
        array('type' => 'text/jscript', 'extension' => 'js'),

        array('type' => 'text/css', 'extension' => 'css'),

        array('type' => 'image/svg+xml', 'extension' => 'svg'),
        array('type' => 'image/jpeg', 'extension' => 'jpg'),
        array('type' => 'image/jpeg', 'extension' => 'jpeg'),
        array('type' => 'image/jpeg', 'extension' => 'jpe'),
        array('type' => 'image/png', 'extension' => 'png'),
        array('type' => 'image/bmp', 'extension' => 'bmp'),
        array('type' => 'image/gif', 'extension' => 'gif'),
        array('type' => 'image/webp', 'extension' => 'webp'),

        array('type' => 'font/woff', 'extension' => 'woff'),
        array('type' => 'font/woff2', 'extension' => 'woff2'),
        array('type' => 'font/ttf', 'extension' => 'ttf'),
        array('type' => 'font/eot', 'extension' => 'eot'),
        array('type' => 'font/otf', 'extension' => 'otf'),
        array('type' => 'application/xml', 'extension' => 'xml'),
        array('type' => 'application/json', 'extension' => 'json'),
        array('type' => 'application/ld+json', 'extension' => 'jsonld'),
        array('type' => 'text/plain', 'extension' => 'txt'),
        array('type' => 'application/rss+xml', 'extension' => 'rss')
    );

    private function findStringInArray($key, $value, $return)
    {
        $res = null;
        foreach (self::MIMES_CDN as $item) {
            if ($item[$key] == $value) {
                $res = $item[$return];
                break;
            }
        }
        return $res;
    }

    public function getExtension(string $mimetype)
    {
        $res = null;
        $results = $this->findStringInArray('type', $mimetype, 'extension');
        if (!empty($results)) {
            $res = $results;
        }
        return $res;
    }
    public function getMimeType(string $extension)
    {
        $res = null;
        $results = $this->findStringInArray('extension', $extension, 'type');
        if (!empty($results)) {
            $res = $results;
        }
        return $res;
    }
}
