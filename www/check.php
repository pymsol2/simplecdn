<?php

use Pymsol\Utilities\File;

define('APP', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR);

// load application config (error reporting etc.)
require APP . '/Config/Config.php';

try {

    print_r(get_loaded_extensions());

    $path = ROOT . 'content';
    createFolder($path);

    $path = ROOT . 'contentExternal';
    createFolder($path);

    $path = ROOT . 'logs_cdn';
    createFolder($path);
} catch (Error $e) {
    echo 'No tengo permisos para generar las carpetas. ' . $ex->getMessage();
}

function createFolder($path)
{
    if ((new File)->exist($path)) {
        echo 'La carpeta existe. ' . $path . '</br>';
    } else {
        echo 'Creando carpeta. ' . $path . '</br>';
        (new File)->makeDirectory($path);
    }
}
