<?php

use Pymsol\SimpleCDN\Application;

define('APP', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR);

// load application config (error reporting etc.)
require APP . '/Config/Config.php';

// launch the application
$app = new Application($_SERVER);
